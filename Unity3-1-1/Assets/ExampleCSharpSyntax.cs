﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExampleCSharpSyntax : MonoBehaviour {

	int myInt = 5;

	int MyFunction(int number)
	{
		int ret = myInt * number;
		return ret;
	}
}
